//
//  BucketsViewController.swift
//  MoneyMan
//
//  Created by Jake Edwards on 3/18/19.
//  Copyright © 2019 Big Ahi Software. All rights reserved.
//

import UIKit
import FirebaseAuth

class BucketsViewController: UIViewController, BucketsUpdateDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    private let TitleLabel: UILabel = {
        let label = UILabel()
        label.text = "My Money Buckets"
        label.textColor = .black
        label.textAlignment = .left
        label.backgroundColor = .clear
        label.font = FontConstants.formTitle
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private let BucketsCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.backgroundColor = .red
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }()
    private let NewBucketButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("+", for: .normal)
        button.setTitleColor(.gray, for: .normal)
        button.backgroundColor = .red
        button.addTarget(self, action: #selector(presentNewBucketButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    private let CellId = "cellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        DbActions.shared.delegate = self
        BucketsCollection.delegate = self
        BucketsCollection.dataSource = self
        BucketsCollection.register(BucketsCollectionViewCell.self, forCellWithReuseIdentifier: CellId)
        
        addSubviewsToView()
        anchorSubviews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let noUserLoggedIn = Auth.auth().currentUser?.uid == nil
        if noUserLoggedIn { handleLogout() }
        NewBucketButton.layer.cornerRadius = NewBucketButton.frame.width/2
        DbActions.shared.fetchBuckets()
    }
    
    private func addSubviewsToView() {
        let subviews = [TitleLabel, BucketsCollection, NewBucketButton]
        subviews.forEach { self.view.addSubview($0) }
    }
    
    private func anchorSubviews() {
        TitleLabel.setTopAnchor(to: self.view.safeAreaLayoutGuide.topAnchor, withPadding: PaddingConstants.titleTop)
        TitleLabel.setLeadingAnchor(to: BucketsCollection.leadingAnchor)
        
        let collectionWidth = self.view.frame.width*FieldConstants.widthMultiplier
        BucketsCollection.setTopAnchor(to: TitleLabel.bottomAnchor, withPadding: PaddingConstants.fieldTop)
        BucketsCollection.setCenterXAnchor(to: self.view.centerXAnchor)
        BucketsCollection.setBottomAnchor(to: self.view.safeAreaLayoutGuide.bottomAnchor, withPadding: -PaddingConstants.titleTop)
        BucketsCollection.setWidthAnchor(to: collectionWidth)
        
        let padding = UIEdgeInsets(top: 0, left: 0, bottom: -self.view.frame.height*0.05, right: -20)
        NewBucketButton.anchor(top: nil, bottom: self.view.safeAreaLayoutGuide.bottomAnchor, leading: nil, trailing: self.view.safeAreaLayoutGuide.trailingAnchor, height: 25, width: 25, padding: padding)       
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let count = DbActions.shared.Buckets.count
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let bucket = DbActions.shared.Buckets[indexPath.row]
        
        let cell = BucketsCollection.dequeueReusableCell(withReuseIdentifier: CellId, for: indexPath) as! BucketsCollectionViewCell
        cell.setBucketTo(bucket)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.view.frame.width*FieldConstants.widthMultiplier
        let height = self.view.frame.height*FieldConstants.heightMultiplier
        let size = CGSize(width: width, height: height)
        return size
    }
    
    @objc private func presentNewBucketButton() {
        self.presentNewBucketVc()
    }
    
    private func handleLogout() {
        let userDidLogout = DbActions.logoutUser()
        if userDidLogout {
            self.presentLoginVc()
        }
    }
    
    func didFetchBuckets() {
        BucketsCollection.reloadData()
    }
}
