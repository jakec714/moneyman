//
//  NewBucketViewController.swift
//  MoneyMan
//
//  Created by Jake Edwards on 4/12/19.
//  Copyright © 2019 Big Ahi Software. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class NewBucketViewController: UIViewController {
    private let TitleLabel: UILabel = {
        let label = UILabel()
        label.text = "New Money Bucket"
        label.textColor = .black
        label.textAlignment = .left
        label.backgroundColor = .clear
        label.font = FontConstants.formTitle
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private let NameField = UnderlinedTextField(title: "Name", placeholder: "Vacation", highlightColor: ColorConstants.highlightColor, underlineColor: ColorConstants.underlineColor)
    private let TargetAmountField = UnderlinedTextField(title: "Goal Amount", placeholder: "$1000", keyboardType: .decimalPad, highlightColor: ColorConstants.highlightColor, underlineColor: ColorConstants.underlineColor)
    private let StartingAmountField = UnderlinedTextField(title: "Starting Amount", placeholder: "$100", keyboardType: .decimalPad, highlightColor: ColorConstants.highlightColor, underlineColor: ColorConstants.underlineColor)
    private let SubmitButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = ColorConstants.highlightColor
        button.setTitle("SUBMIT", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 25
        button.layer.shadowOffset = CGSize(width: 0, height: 10)
        button.layer.shadowRadius = 8
        button.layer.shadowColor = ColorConstants.highlightColor.cgColor
        button.layer.shadowOpacity = 0.25
        button.addTarget(self, action: #selector(handleSubmit), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    private let CancelButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .clear
        button.setTitle("Cancel", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(handleCancel), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addSubviewsToView()
        anchorSubviews()
    }
    
    private func addSubviewsToView() {
        let subviews = [TitleLabel, NameField, TargetAmountField, StartingAmountField, SubmitButton, CancelButton]
        subviews.forEach { self.view.addSubview($0) }
    }
    
    private func anchorSubviews() {
        TitleLabel.setTopAnchor(to: self.view.safeAreaLayoutGuide.topAnchor, withPadding: PaddingConstants.titleTop)
        TitleLabel.setLeadingAnchor(to: NameField.leadingAnchor)
        
        let fieldHeight = self.view.frame.height*FieldConstants.heightMultiplier
        let nameFieldWidth: CGFloat = self.view.frame.width*FieldConstants.widthMultiplier
        
        NameField.setTopAnchor(to: TitleLabel.bottomAnchor, withPadding: PaddingConstants.fieldTop)
        NameField.setCenterXAnchor(to: self.view.centerXAnchor)
        NameField.setHeightAnchor(to: fieldHeight)
        NameField.setWidthAnchor(to: nameFieldWidth)
        
        let padding: CGFloat = 20
        let numberFieldWidth = (nameFieldWidth*0.50)-padding
        
        TargetAmountField.setTopAnchor(to: NameField.bottomAnchor, withPadding: PaddingConstants.fieldTop)
        TargetAmountField.setLeadingAnchor(to: NameField.leadingAnchor)
        TargetAmountField.setHeightAnchor(to: fieldHeight)
        TargetAmountField.setWidthAnchor(to: numberFieldWidth)
        
        StartingAmountField.setTopAnchor(to: NameField.bottomAnchor, withPadding: PaddingConstants.fieldTop)
        StartingAmountField.setTrailingAnchor(to: NameField.trailingAnchor)
        StartingAmountField.setHeightAnchor(to: fieldHeight)
        StartingAmountField.setWidthAnchor(to: numberFieldWidth)
        
        SubmitButton.setTopAnchor(to: TargetAmountField.bottomAnchor, withPadding: PaddingConstants.submitTop)
        SubmitButton.setCenterXAnchor(to: NameField.centerXAnchor)
        SubmitButton.setHeightAnchor(to: fieldHeight)
        SubmitButton.setWidthAnchor(to: nameFieldWidth)
        
        CancelButton.setTopAnchor(to: SubmitButton.bottomAnchor, withPadding: PaddingConstants.submitBottom)
        CancelButton.setCenterXAnchor(to: NameField.centerXAnchor)
    }
    
    @objc private func handleSubmit() {
        let name = NameField.getText()
        let targetAmount = TargetAmountField.getInt()
        let startingAmount = StartingAmountField.getInt()
        let fieldsHaveValues = name.count > 0 && targetAmount > 0 && startingAmount >= 0
        if !fieldsHaveValues { return }
        let values: [String: Any] = ["name": name, "targetAmount": targetAmount, "currentAmount": startingAmount]
        
        guard let userId = Auth.auth().currentUser?.uid else { return }
        let userRef = Database.database().reference(withPath: "users").child(userId)
        let currentUserBucketsRef = userRef.child("buckets").childByAutoId()
        
        currentUserBucketsRef.setValue(values, withCompletionBlock: { (userUpdateError, ref) in
            let errorAddingUser = userUpdateError != nil
            if errorAddingUser { return }
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    @objc private func handleCancel() {
        self.dismiss(animated: true, completion: nil)
    }
}
