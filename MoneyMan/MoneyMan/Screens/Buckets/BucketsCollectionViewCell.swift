//
//  BucketsCollectionViewCell.swift
//  MoneyMan
//
//  Created by Jake Edwards on 4/13/19.
//  Copyright © 2019 Big Ahi Software. All rights reserved.
//

import UIKit

class BucketsCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = .black
        self.layer.cornerRadius = 8
    }
    
    public func setBucketTo(_ bucket: BucketModel) {
        print(bucket.Name)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
