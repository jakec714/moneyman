//
//  UiExtensions.swift
//  MoneyMan
//
//  Created by Jake Edwards on 3/3/19.
//  Copyright © 2019 Big Ahi Software. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func presentLoginVc() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let loginVc = storyBoard.instantiateViewController(withIdentifier: "LoginVc")
        self.present(loginVc, animated: true, completion: nil)
    }
}

extension UIView {
    func setTopAnchor(to anchor: NSLayoutYAxisAnchor, withPadding padding: CGFloat = 0) {
        topAnchor.constraint(equalTo: anchor, constant: padding).isActive = true
    }
    
    func setBottomAnchor(to anchor: NSLayoutYAxisAnchor, withPadding padding: CGFloat = 0) {
        bottomAnchor.constraint(equalTo: anchor, constant: padding).isActive = true
    }
    
    func setLeadingAnchor(to anchor: NSLayoutXAxisAnchor, withPadding padding: CGFloat = 0) {
        leadingAnchor.constraint(equalTo: anchor, constant: padding).isActive = true
    }
    
    func setTrailingAnchor(to anchor: NSLayoutXAxisAnchor, withPadding padding: CGFloat = 0) {
        trailingAnchor.constraint(equalTo: anchor, constant: padding).isActive = true
    }
    
    func setCenterXAnchor(to anchor: NSLayoutXAxisAnchor, withPadding padding: CGFloat = 0) {
        centerXAnchor.constraint(equalTo: anchor, constant: padding).isActive = true
    }
    
    func setCenterYAnchor(to anchor: NSLayoutYAxisAnchor) {
        centerYAnchor.constraint(equalTo: anchor).isActive = true
    }
    
    func setWidthAnchor(to anchor: NSLayoutDimension, multiplier: CGFloat = 1, constant: CGFloat = 0) {
        widthAnchor.constraint(equalTo: anchor, multiplier: multiplier, constant: constant)
    }
    
    func setHeightAnchor(to anchor: NSLayoutDimension, multiplier: CGFloat = 1, constant: CGFloat = 0) {
        heightAnchor.constraint(equalTo: anchor, multiplier: multiplier, constant: constant)
    }
    
    func setWidthAnchor(to width: CGFloat) {
        widthAnchor.constraint(equalToConstant: width).isActive = true
    }
    
    func setHeightAnchor(to height: CGFloat) {
        heightAnchor.constraint(equalToConstant: height).isActive = true
    }
    
    func anchor(top: NSLayoutYAxisAnchor?, bottom: NSLayoutYAxisAnchor?, leading: NSLayoutXAxisAnchor?, trailing: NSLayoutXAxisAnchor?, height: CGFloat?, width: CGFloat?, padding: UIEdgeInsets = .zero) {
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: padding.top).isActive = true
        }
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: padding.bottom).isActive = true
        }
        if let trailing = trailing {
            trailingAnchor.constraint(equalTo: trailing, constant: padding.right).isActive = true
        }
        if let leading = leading {
            leadingAnchor.constraint(equalTo: leading, constant: padding.left).isActive = true
        }
        if let height = height {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
        if let width = width {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
    }
    
    func createYConstraint(on anchorOfView: NSLayoutYAxisAnchor, equalTo anchorOfOtherView: NSLayoutYAxisAnchor, constant: CGFloat) -> NSLayoutConstraint {
        let constraint = anchorOfView.constraint(equalTo: anchorOfOtherView, constant: constant)
        constraint.isActive = true
        return constraint
    }
    /*
    func createXConstraint(on anchorOfView: NSLayoutXAxisAnchor, equalTo anchorOfOtherView: NSLayoutXAxisAnchor, constant: CGFloat) -> NSLayoutConstraint {
        let constraint = anchorOfView.constraint(equalTo: anchorOfOtherView, constant: constant)
        constraint.isActive = true
        return constraint
    }
 
    
    */
    
}

extension UIColor {
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
}
