//
//  IUnderlinedTextField.swift
//  MoneyMan
//
//  Created by Jake Edwards on 3/27/19.
//  Copyright © 2019 Big Ahi Software. All rights reserved.
//

import Foundation
import UIKit

protocol IUnderlinedTextField {
    var Title: UILabel { get set }
    var Field: UITextField { get set }
    var Underline: UIView { get set }
    var SelectedUnderlineColor: UIColor { get set }
    func reset()
    func focus()
    func getText() -> String
    func setupSelf()
    func anchorSubviews()
}
