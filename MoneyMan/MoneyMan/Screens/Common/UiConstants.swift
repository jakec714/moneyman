//
//  UiConstants.swift
//  MoneyMan
//
//  Created by Jake Edwards on 4/10/19.
//  Copyright © 2019 Big Ahi Software. All rights reserved.
//

import Foundation
import UIKit

struct ColorConstants {
    static let highlightColor = UIColor.purple
    static let underlineColor = UIColor.lightGray
}

struct TextConstants {
    static let loginTitle = "Sign In"
    static let loginSubmit = "SIGN IN"
    static let changeToLogin = "New?"
    
    static let registerTitle = "Register"
    static let registerSubmit = "REGISTER"
    static let changeToRegister = "Already have an account?"
}

struct FontConstants {
    static let formTitle = UIFont.boldSystemFont(ofSize: 30)
}

struct FieldConstants {
    static let widthMultiplier: CGFloat = 0.80
    static let heightMultiplier: CGFloat = 0.055
}

struct PaddingConstants {
    static let titleTop: CGFloat = 30
    
    static let fieldTop: CGFloat = 20
    
    static let submitTop: CGFloat = 25
    static let submitBottom: CGFloat = 10
}
