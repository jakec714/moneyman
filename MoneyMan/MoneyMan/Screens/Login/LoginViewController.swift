//
//  LoginViewController.swift
//  MoneyMan
//
//  Created by Jake Edwards on 4/8/19.
//  Copyright � 2019 Big Ahi Software. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class LoginViewController: UIViewController {
    private let TitleLabel: UILabel = {
        let label = UILabel()
        label.text = "title"
        label.textColor = .black
        label.textAlignment = .left
        label.backgroundColor = .clear
        label.font = FontConstants.formTitle
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    private let NameField = UnderlinedTextField(title: "Name", placeholder: "John Doe", highlightColor: ColorConstants.highlightColor, underlineColor: ColorConstants.underlineColor)
    private let EmailField = UnderlinedTextField(title: "Email Address", placeholder: "john.doe@example.com", highlightColor: ColorConstants.highlightColor, underlineColor: ColorConstants.underlineColor)
    private let PasswordField = UnderlinedTextField(title: "Password", placeholder: "* * * *", highlightColor: ColorConstants.highlightColor, underlineColor: ColorConstants.underlineColor)
    private let SubmitButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = ColorConstants.highlightColor
        button.setTitle("submit", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 25
        button.layer.shadowOffset = CGSize(width: 0, height: 10)
        button.layer.shadowRadius = 8
        button.layer.shadowColor = ColorConstants.highlightColor.cgColor
        button.layer.shadowOpacity = 0.25
        button.addTarget(self, action: #selector(handleSubmit), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    private let SwitchModesButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .clear
        button.setTitle("switch mode", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(changeMode), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private var SubmitModeIsSignIn = true
    private var NameFieldHeightAnchor: NSLayoutConstraint?
    private var FieldHeight: CGFloat!
    
    override func viewDidLoad() {
        FieldHeight = self.view.frame.height*FieldConstants.heightMultiplier
        
        super.viewDidLoad()
        
        addSubviewsToView()
        anchorSubViews()
        
        setModeAttributes()
    }
    
    private func addSubviewsToView() {
        let subviews = [TitleLabel, NameField, EmailField, PasswordField, SubmitButton, SwitchModesButton]
        subviews.forEach { self.view.addSubview($0) }
    }
    
    private func anchorSubViews() {
        TitleLabel.setTopAnchor(to: self.view.safeAreaLayoutGuide.topAnchor, withPadding: PaddingConstants.titleTop)
        TitleLabel.setLeadingAnchor(to: NameField.leadingAnchor)
        
        let fieldWidth: CGFloat = self.view.frame.width*FieldConstants.widthMultiplier
        
        NameField.setTopAnchor(to: TitleLabel.bottomAnchor, withPadding: PaddingConstants.fieldTop)
        NameField.setCenterXAnchor(to: self.view.centerXAnchor)
        NameFieldHeightAnchor = NameField.heightAnchor.constraint(equalToConstant: FieldHeight)
        NameFieldHeightAnchor?.isActive = true
        NameField.setWidthAnchor(to: fieldWidth)
        
        EmailField.setTopAnchor(to: NameField.bottomAnchor, withPadding: PaddingConstants.fieldTop)
        EmailField.setCenterXAnchor(to: NameField.centerXAnchor)
        EmailField.setHeightAnchor(to: FieldHeight)
        EmailField.setWidthAnchor(to: fieldWidth)
        
        PasswordField.setTopAnchor(to: EmailField.bottomAnchor, withPadding: PaddingConstants.fieldTop)
        PasswordField.setCenterXAnchor(to: NameField.centerXAnchor)
        PasswordField.setHeightAnchor(to: FieldHeight)
        PasswordField.setWidthAnchor(to: fieldWidth)
        
        SubmitButton.setTopAnchor(to: PasswordField.bottomAnchor, withPadding: PaddingConstants.submitTop)
        SubmitButton.setCenterXAnchor(to: NameField.centerXAnchor)
        SubmitButton.setHeightAnchor(to: FieldHeight)
        SubmitButton.setWidthAnchor(to: fieldWidth)
        
        SwitchModesButton.setTopAnchor(to: SubmitButton.bottomAnchor, withPadding: PaddingConstants.submitBottom)
        SwitchModesButton.setCenterXAnchor(to: NameField.centerXAnchor)
    }
    
    @objc private func handleSubmit() {
        SubmitModeIsSignIn ? loginUser() : registerUser()
    }
    
    private func registerUser() {
        let name = NameField.getText()
        let email = EmailField.getText()
        let password = PasswordField.getText()
        let fieldsHaveValues = name.count > 0 && email.count > 0 && password.count > 0
        if !fieldsHaveValues { return }
        
        let registerUserSuccess = DbActions.registerUserWith(name: name, email: email, password: password)
        if registerUserSuccess {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    private func loginUser() {
        let email = EmailField.getText()
        let password = PasswordField.getText()
        let fieldsHaveValues = email.count > 0 && password.count > 0
        if !fieldsHaveValues { return }
        
        let signInSuccess = DbActions.loginUserWith(email: email, password: password)
        if signInSuccess {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc private func changeMode() {
        SubmitModeIsSignIn = !SubmitModeIsSignIn
        setModeAttributes()
    }
    
    private func setModeAttributes() {
        let submitTitle = SubmitModeIsSignIn ? TextConstants.loginSubmit : TextConstants.registerSubmit
        let switchModesTitle = SubmitModeIsSignIn ? TextConstants.changeToLogin : TextConstants.changeToRegister
        let titleText = SubmitModeIsSignIn ? TextConstants.loginTitle : TextConstants.registerTitle
        let nameFieldHeightConstant: CGFloat = SubmitModeIsSignIn ? 0 : FieldHeight
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.TitleLabel.text = titleText
            self.SubmitButton.setTitle(submitTitle, for: .normal)
            self.SwitchModesButton.setTitle(switchModesTitle, for: .normal)
            self.NameFieldHeightAnchor?.constant = nameFieldHeightConstant
            
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}
