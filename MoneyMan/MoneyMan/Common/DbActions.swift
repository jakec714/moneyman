//
//  DbActions.swift
//  MoneyMan
//
//  Created by Jake Edwards on 3/15/19.
//  Copyright © 2019 Big Ahi Software. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth

class DbActions {
    static let shared = DbActions()
    weak var delegate: BucketsUpdateDelegate?
    
    private(set) var Buckets: [BucketModel] = []
    
    public static func addBucket(_ bucket: BucketModel) {
        let ref = Database.database().reference().child("buckets")
        ref.childByAutoId().setValue(bucket.toJson())
    }
    
    public func fetchBuckets() {                
        var tempBuckets: [BucketModel] = []
        if let userId = Auth.auth().currentUser?.uid {
            let userBucketsRef = Database.database().reference().child("users").child(userId).child("buckets")
            userBucketsRef.observeSingleEvent(of: .value, with: { (snapshot) in
                for child in snapshot.children {
                    if let snap = child as? DataSnapshot, let bucket = BucketModel(snapshot: snap) {
                        tempBuckets.append(bucket)
                    } else { print("didnt make it")}
                }
                self.Buckets = tempBuckets
                self.delegate?.didFetchBuckets()
            })
        }
    }
    
    public static func registerUserWith(name: String, email: String, password: String) -> Bool {
        var registerUserSuccess = true
        Auth.auth().createUser(withEmail: email, password: password, completion: {(result: AuthDataResult?, userCreateError) in
            let errorCreatingUser = userCreateError != nil
            if errorCreatingUser { registerUserSuccess = false }
            
            if let userId = result?.user.uid {
                let addUserSuccess = addUserWith(uid: userId, name: name, email: email)
                registerUserSuccess = addUserSuccess
            } else { registerUserSuccess = false }
        })
        return registerUserSuccess
    }
    
    private static func addUserWith(uid: String, name: String, email: String) -> Bool {
        var addUserSuccess = true
        let usersRef = Database.database().reference(withPath: "users")
        let createdUserRef = usersRef.child(uid)
        let values = ["name": name, "email": email]
        
        createdUserRef.updateChildValues(values, withCompletionBlock: { (userUpdateError, ref) in
            let errorAddingUser = userUpdateError != nil
            if errorAddingUser { addUserSuccess = false }
        })
        return addUserSuccess
    }
    
    public static func loginUserWith(email: String, password: String) -> Bool {
        var signInSuccess = true
        Auth.auth().signIn(withEmail: email, password: password) { (result: AuthDataResult?, signInError) in
            let errorLogginInUser = signInError != nil
            if errorLogginInUser { signInSuccess = false }
        }
        return signInSuccess
    }
    
    public static func logoutUser() -> Bool {
        do {
            try Auth.auth().signOut()
        } catch let logoutError {
            print(logoutError)
            return false
        }
        return true
    }
}
