//
//  Delegates.swift
//  MoneyMan
//
//  Created by Jake Edwards on 4/13/19.
//  Copyright © 2019 Big Ahi Software. All rights reserved.
//

import Foundation

protocol BucketsUpdateDelegate: class {
    func didFetchBuckets()
}
