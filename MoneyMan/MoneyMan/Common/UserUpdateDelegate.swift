//
//  UpdateDelegate.swift
//  MoneyMan
//
//  Created by Jake Edwards on 3/9/19.
//  Copyright © 2019 Big Ahi Software. All rights reserved.
//

import Foundation

protocol UserUpdateDelegate: class {
    func didUpdateAccounts()
}
