//
//  Account.swift
//  MoneyMan
//
//  Created by Jake Edwards on 3/3/19.
//  Copyright © 2019 Big Ahi Software. All rights reserved.
//

import Foundation
import FirebaseDatabase

class BucketModel {
    public var Ref: DatabaseReference?
    public var Key: String?
    
    public var Name: String
    public var CurrentAmount: Double
    public var TargetAmount: Int
    
    public init?(snapshot: DataSnapshot) {
        guard let values = snapshot.value as? [String: AnyObject],
            let name = values["name"] as? String,
            let currentAmount = values["currentAmount"] as? Double,
            let targetAmount = values["targetAmount"] as? Int
            else { return nil }
        
        Ref = snapshot.ref
        Key = snapshot.key
        
        Name = name
        CurrentAmount = currentAmount
        TargetAmount = targetAmount
    }
    
    public init(name: String, currentAmount: Double = 0, targetAmount: Int) {
        Name = name
        CurrentAmount = currentAmount
        TargetAmount = targetAmount
    }
    
    public func toJson() -> [String: Any] {
        let json: [String: Any] = ["name": Name, "currentAmount": CurrentAmount, "targetAmount": TargetAmount]
        return json
    }
}
